import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import ColorRandomizer from '../helpers/ColorRandomizer';

export default function DoughnutChart({ label, expenseArray, incomeArray }) {
  const [bgColors, setBgColors] = useState([]);
  useEffect(() => {
    setBgColors(incomeArray.map(() => `#${ColorRandomizer()}`));
  }, [label]);
  const data = {
    labels: label,
    datasets: [
      {
        label: 'Income',
        data: incomeArray,
        fill: false,
        backgroundColor: bgColors,
        // borderColor: 'rgba(255, 99, 0, 0.5)',
      },
      {
        label: 'Expense',
        data: expenseArray,
        fill: false,
        backgroundColor: bgColors,
        // borderColor: 'rgba(255, 99, 0, 0.5)',
      },
    ],
  };

  return <Pie data={data} />;
}
