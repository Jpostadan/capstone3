import React from 'react';

import { Table, Card } from 'react-bootstrap';

import moment from 'moment';

export default function ShowRecords({ records, category }) {
  return (
    <React.Fragment>
      <Card>
        <Card.Header className="text-center">Records History</Card.Header>

        <Table striped bordered hover responsive className="text-center">
          <thead>
            <tr>
              <th>Date</th>
              <th>Name</th>
              <th>Description</th>
              <th>Type</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {records.reverse().map((record) => {
              if (category === '') {
                return (
                  <tr key={record._id}>
                    <td>{moment(record.inputOn).format('MMMM DD, YYYY')}</td>
                    <td>{record.categoryName}</td>
                    <td>{record.description}</td>
                    <td>{record.categoryType}</td>
                    <td>
                      {record.amount
                        .toString()
                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                    </td>
                  </tr>
                );
              } else if (
                category === 'Income' &&
                category === record.categoryType
              ) {
                return (
                  <tr key={record._id}>
                    <td>{moment(record.inputOn).format('MMMM DD, YYYY')}</td>
                    <td>{record.categoryName}</td>
                    <td>{record.description}</td>
                    <td>{record.categoryType}</td>
                    <td>
                      {record.amount
                        .toString()
                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                    </td>
                  </tr>
                );
              } else if (
                category === 'Expense' &&
                category === record.categoryType
              ) {
                return (
                  <tr key={record._id}>
                    <td>{moment(record.inputOn).format('MMMM DD, YYYY')}</td>
                    <td>{record.categoryName}</td>
                    <td>{record.description}</td>
                    <td>{record.categoryType}</td>
                    <td>
                      {record.amount
                        .toString()
                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </Table>
      </Card>
    </React.Fragment>
  );
}
