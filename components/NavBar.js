import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';

import UserContext from '../UserContext';

export default function NavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="info" expand="lg" className="sticky">
      <Link href="/">
        <a className="navbar-brand">Track</a>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {user.id !== null ? (
            user.isActive === true ? (
              <Link>
                <Link href="/dashboard">
                  <a className="nav-link" role="button">
                    Dashboard
                  </a>
                </Link>
                <Link href="/records">
                  <a className="nav-link" role="button">
                    Transactions
                  </a>
                </Link>
                <Link href="/trends">
                  <a className="nav-link" role="button">
                    Trends
                  </a>
                </Link>
                <Link href="/breakdown">
                  <a className="nav-link" role="button">
                    Breakdown
                  </a>
                </Link>
                <Link href="/logout">
                  <a className="nav-link" role="button">
                    Logout
                  </a>
                </Link>
                <Link href="/categories">
                  <a className="nav-link" role="button">
                    Categories
                  </a>
                </Link>
              </Link>
            ) : (
              <>
                <Link href="/dashboard">
                  <a className="nav-link" role="button">
                    Categories
                  </a>
                </Link>

                <Link href="/records">
                  <a className="nav-link" role="button">
                    Transactions
                  </a>
                </Link>

                <Link href="/trends">
                  <a className="nav-link" role="button">
                    Trends
                  </a>
                </Link>
                <Link href="/breakdown">
                  <a className="nav-link" role="button">
                    Breakdown
                  </a>
                </Link>
                <Link href="/categories">
                  <a className="nav-link" role="button">
                    Categories
                  </a>
                </Link>

                <Link href="/logout">
                  <a className="nav-link" role="button">
                    Logout
                  </a>
                </Link>
              </>
            )
          ) : (
            <>
              <Link href="/login">
                <a className="nav-link" role="button">
                  Login
                </a>
              </Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
