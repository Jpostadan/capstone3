import { useContext } from 'react';
import UserContext from '../UserContext';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
const year = new Date().getFullYear().toString();

export default function MonthlyIncome() {
  const { user } = useContext(UserContext);

  const income = [];
  months.forEach((month) => {
    let sum = 0;
    user.records.forEach((record) => {
      if (
        record.categoryType.toLowerCase() == 'income' &&
        moment(record.inputOn).format('YYYY') == year &&
        moment(record.inputOn).format('MMMM') == month
      )
        sum += record.amount;
    });
    income.push(sum);
  });

  const data = {
    labels: months,
    datasets: [
      {
        label: `Monthly Income for ${year}`,
        data: income,
        backgroundColor: 'rgba(0, 255, 0, 0.3)',
      },
    ],
  };

  return (
    <div>
      <h3 className="text-center">Monthly Income</h3>
      <Bar data={data} />
    </div>
  );
}
