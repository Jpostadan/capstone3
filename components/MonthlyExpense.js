import { useContext } from 'react';
import UserContext from '../UserContext';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
const year = new Date().getFullYear().toString();

export default function MonthlyExpense() {
  const { user } = useContext(UserContext);
  const expenses = [];
  months.forEach((month) => {
    let sum = 0;
    user.records.forEach((record) => {
      if (
        record.categoryType.toLowerCase() == 'expense' &&
        moment(record.inputOn).format('YYYY') == year &&
        moment(record.inputOn).format('MMMM') == month
      )
        sum += record.amount;
    });
    expenses.push(sum);
  });

  console.log(expenses);

  const data = {
    labels: months,
    datasets: [
      {
        label: `Monthly Expenses for ${year}`,
        data: expenses,
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
    ],
  };

  return (
    <div>
      <h3 className="text-center">Monthly Expenses</h3>
      <Bar data={data} />
    </div>
  );
}
