import { Line } from 'react-chartjs-2';

export default function LineChart({ dateArray, incomeArray, expenseArray }) {
  const data = {
    labels: dateArray,
    datasets: [
      {
        label: 'Income',
        data: incomeArray,
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(0, 255, 0, 0.5)',
      },
      {
        label: 'Expense',
        data: expenseArray,
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(255, 99, 0, 0.5)',
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return <Line data={data} options={options} />;
}
