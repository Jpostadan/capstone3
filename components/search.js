import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Form } from 'react-bootstrap';

import View from './View';
import ShowRecords from './showRecords';

export default function index() {
  return (
    <View title={'Budget Tracker: Records History'}>
      <Row className="justify-content-center">
        <Col className="text-center">
          <Search />
        </Col>
      </Row>
    </View>
  );
}

const Search = () => {
  const [records, setRecords] = useState([]);
  const [search, setSearch] = useState('');
  const [category, SetCategory] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  useEffect(() => {
    const options = {
      headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    };

    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details`,
      options
    )
      .then((res) => res.json())
      .then((data) => {
        const records = data.records;
        setRecords(records);
      });
  }, []);

  useEffect(() => {
    console.log(category);
  }, [category]);

  const filteredName =
    search.length === 0
      ? records
      : records.filter((record) =>
          record.categoryName.toLowerCase().includes(search.toLowerCase())
        );

  return (
    <React.Fragment>
      <Card className="p-3 mb-3">
        <Form onSubmit={(e) => addCategory(e)}>
          <Form.Row className="justify-content-md-center my-2">
            <Form.Group controlId="searchName">
              <Form.Control
                type="text"
                placeholder="Search name"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                required
              />
            </Form.Group>
            <Col sm="6" md="3">
              <Form.Control
                onChange={(e) => SetCategory(e.target.value)}
                as="select"
                required
              >
                <option value="">All</option>
                <option value="Income">Income</option>
                <option value="Expense">Expense</option>
              </Form.Control>
            </Col>

            <Col md="auto">
              <Form.Label>Date:</Form.Label>
            </Col>
            <Col>
              <Form.Control
                type="date"
                dateformat="yyyy-MM-DD"
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
              />
            </Col>
            <Col md="auto">
              <Form.Label> to </Form.Label>
            </Col>
            <Col>
              <Form.Control
                type="date"
                value={endDate}
                dateformat="myyyy-MM-DD"
                onChange={(e) => setEndDate(e.target.value)}
              />
            </Col>
          </Form.Row>
        </Form>
      </Card>
      <ShowRecords records={filteredName} category={category} />
    </React.Fragment>
  );
};
