import { useContext } from 'react';
import { Form, Col, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function Banner({ dataProp }) {
  const data = {
    title: 'Welcome to Nxt Budget Tracker!',
  };

  const { token } = useContext(UserContext);

  function guest() {
    if (token === null) {
      Swal.fire(
        'Authentication Failed',
        'You need to Login first. If you do not have an account, please register.',
        'warning'
      );
      Router.push('/login');
    } else {
      Router.push('/addRecord');
    }
  }

  return (
    <Form>
      <Col className="align-left mt-4 pt-4"></Col>
      <Form.Row>
        <Col>
          <Button className="float-center ml-4" onClick={guest}>
            Add Record
          </Button>
        </Col>
      </Form.Row>
    </Form>
  );
}
