import { useState, useEffect } from 'react';
import { Form, Button, Container, Jumbotron, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

function categoryModal(props) {
  const [categoryType, setCategoryType] = useState('income');
  const [categoryName, setCategoryName] = useState('');

  const [categories, setCategories] = useState('');
  const [isActive, setIsActive] = useState(false);

  function category(e) {
    e.preventDefault();
    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/new-category`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        categoryName: categoryName,
        categoryType: categoryType,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire('Category added successfully!', '', 'success');
        } else {
          Swal.fire('Incomplete', '', 'warning');
        }
      });
  }

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/categories`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        let number = 0;
        const newArray = data.map((category) => {
          console.log(category);
          number++;
          return (
            <tr key={category._id}>
              <td>{number}</td>
              <td>{category.categoryName}</td>
              <td>{category.categoryType}</td>
            </tr>
          );
        });
        setCategories(newArray);
      });
  }, []);

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <Container>
          <Form onSubmit={(e) => category(e)}>
            <Jumbotron>
              <h1 className="text-center"> Add Transaction Category</h1>
              <Form.Group>
                <Form.Label className="mt-3">Category Type</Form.Label>
                <Form.Control
                  as="select"
                  value={categoryType}
                  onChange={(e) => setCategoryType(e.target.value)}
                  required
                >
                  <option value="income">Income</option>
                  <option value="expense">Expense</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label className="mt-3">Category Name</Form.Label>
                <Form.Control
                  type="categoryName"
                  placeholder="Category Name"
                  value={categoryName}
                  onChange={(e) => setCategoryName(e.target.value)}
                  required
                />
                <Button
                  className="mt-3"
                  variant="primary"
                  type="submit"
                  id="submitBtn"
                  block
                >
                  Submit
                </Button>
              </Form.Group>
            </Jumbotron>
          </Form>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default categoryModal;
