import { Doughnut } from 'react-chartjs-2';

export default function DoughnutChart({ income, expense }) {
  return (
    <Doughnut
      data={{
        datasets: [
          {
            data: [income, expense],
            backgroundColor: [
              'rgba(0, 255, 0, 0.3)',
              'rgba(255, 99, 132, 0.5)',
            ],
          },
        ],
        labels: ['Total Income', 'Total Expense'],
      }}
      redraw={false}
    />
  );
}
