import React, { useState, useEffect } from 'react';
import { Form, Button, Col, Jumbotron } from 'react-bootstrap';
import Router from 'next/router';
import Head from 'next/head';

export default function Register() {
  const [email, setEmail] = useState('');
  const [fName, setFname] = useState('');
  const [lName, setLname] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (password1 !== '' && password2 !== '' && password2 === password1) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [password1, password2]);

  function registerUser(e) {
    e.preventDefault();

    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/email-exists`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          fetch(`https://agile-fortress-85999.herokuapp.com/api/users`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: fName,
              lastName: lName,
              email: email,
              password: password1,
              mobileNo: mobileNo,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data === true) {
                Router.push('/login');
              } else {
                Router.push('/error');
              }
            });
        } else {
          Router.push('/error');
        }
      });
  }

  return (
    <Jumbotron border="info" className="mt-2">
      <Head>
        <title>Register</title>
      </Head>
      <h1 className="text-center mx-2">Create a New Account</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Row>
          <Form.Group as={Col} controlId="userFname">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="name"
              placeholder="Enter First Name"
              value={fName}
              onChange={(e) => setFname(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group as={Col} controlId="userLname">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="name"
              placeholder="Enter Last Name"
              value={lName}
              onChange={(e) => setLname(e.target.value)}
              required
            />
          </Form.Group>
        </Form.Row>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="mobileNo">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Mobile Number"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Row>
          <Form.Group as={Col} controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group as={Col} controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              required
            />
          </Form.Group>
        </Form.Row>
        <Form.Check
          type="checkbox"
          label="I accept the Terms of Use & Privacy Policy"
        />
        {isActive ? (
          <Button
            className="mt-4"
            variant="primary"
            type="submit"
            id="submitBtn"
            block
          >
            Sign up
          </Button>
        ) : (
          <Button
            className="mt-4"
            variant="primary"
            type="submit"
            id="submitBtn"
            disabled
            block
          >
            Sign up
          </Button>
        )}
      </Form>
    </Jumbotron>
  );
}
