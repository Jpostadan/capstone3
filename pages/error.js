import React from 'react';
import Head from 'next/head';
import Banner from '../components/Banner';

export default function error() {
  const errorText = {
    title: 'Error',
    content: 'Something Went Wrong.',
  };

  return (
    <React.Fragment>
      <Head>
        <title>Oops...</title>
      </Head>
      <Banner data={errorText} />
    </React.Fragment>
  );
}
