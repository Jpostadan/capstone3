import React from 'react';
import { useState, useEffect } from 'react';
import { Form, Button, Container, Jumbotron, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function addCategory() {
  const [categoryType, setCategoryType] = useState('income');
  const [categoryName, setCategoryName] = useState('');
  const [categoryList, setCategoryList] = useState('');
  const [categories, setCategories] = useState('');
  const [isActive, setIsActive] = useState(false);

  function category(e) {
    e.preventDefault();
    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/new-category`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        categoryName: categoryName,
        categoryType: categoryType,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire('Category added successfully!', '', 'success');
        } else {
          Swal.fire('Incomplete', '', 'warning');
        }
      });
  }

  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details/categories`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        let number = 0;
        const newArray = data.map((category) => {
          console.log(category);
          number++;
          return (
            <tr key={category._id}>
              <td>{number}</td>
              <td>{category.categoryName}</td>
              <td>{category.categoryType}</td>
            </tr>
          );
        });
        setCategories(newArray);
      });
  }, []);

  return (
    <React.Fragment>
      <Container>
        <Form onSubmit={(e) => category(e)}>
          <Jumbotron className="lg-8 bg-info mt-4">
            <h1 className="text-center">Add Transaction Category</h1>
            <Form.Group>
              <Form.Label className="mt-3">Category Type</Form.Label>
              <Form.Control
                as="select"
                value={categoryType}
                onChange={(e) => setCategoryType(e.target.value)}
                required
              >
                <option value="income">Income</option>
                <option value="expense">Expense</option>
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label className="mt-3">Category Name</Form.Label>
              <Form.Control
                type="categoryName"
                placeholder="Category Name"
                value={categoryName}
                onChange={(e) => setCategoryName(e.target.value)}
                required
              />
              <Button
                className="mt-3 bg-dark"
                type="submit"
                id="submitBtn"
                block
              >
                Submit
              </Button>
            </Form.Group>
          </Jumbotron>

          <Form.Group>
            <Jumbotron
              className="lg-8 bg-info mt-4 text-center"
              type="categoryList"
              value={categoryList}
              onChange={(e) => setCategoryList(e.target.value)}
            >
              <Form.Label>
                <h3 className="text-white">Category List</h3>
              </Form.Label>
              <Table striped bordered hover variant="dark">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Category Name</th>
                    <th>Category Type</th>
                  </tr>
                </thead>
                <tbody>{categories}</tbody>
              </Table>
            </Jumbotron>
          </Form.Group>
        </Form>
      </Container>
    </React.Fragment>
  );
}
