import React from 'react';
import MonthlyExpense from '../components/MonthlyExpense';
import MonthlyIncome from '../components/MonthlyIncome';
import Doughnut from '../components/DoughnutChart';
import { useState, useEffect } from 'react';
import { Card, Container, Col, Row, Jumbotron } from 'react-bootstrap';

export default function trend() {
  const [totalIncome, setTotalIncome] = useState(0);
  const [totalExpense, setTotalExpense] = useState(0);
  const [total, setTotal] = useState(totalIncome, totalExpense);

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/records`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.length) {
          let totalin = 0;
          const incomeArray = data.filter((category) => {
            return category.categoryType === 'Income';
          });
          incomeArray.forEach((category) => {
            totalin += category.amount;
          });

          let totalex = 0;
          const expenseArray = data.filter((category) => {
            return category.categoryType === 'Expense';
          });
          expenseArray.forEach((category) => {
            totalex += category.amount;
          });
          setTotal(totalin - totalex);
          setTotalIncome(totalin);
          setTotalExpense(totalex);
        }
      });
  }, []);

  return (
    <React.Fragment>
      <Jumbotron className="lg-8 bg-info mt-4">
        <Card>
          <Card.Header className="text-center">
            <h1>Savings :{total}</h1>
          </Card.Header>
        </Card>
        <Row className="text-center mt-2">
          <Col>
            <Card>
              <h2>Total Income: {totalIncome}</h2>
            </Card>
          </Col>
          <Col>
            <Card>
              <h2>Total Expense: {totalExpense}</h2>
            </Card>
          </Col>
        </Row>
        <Jumbotron className="lg-8  mt-4">
          <Row>
            <Col>
              <MonthlyExpense />
            </Col>
            <Col>
              <MonthlyIncome />
            </Col>
          </Row>
        </Jumbotron>
        <Jumbotron className="lg-8  mt-4">
          <h2 className="text-center">Tolal Income over Total expense</h2>
          <Container>
            <Doughnut income={totalIncome} expense={totalExpense} />
          </Container>
        </Jumbotron>
      </Jumbotron>
    </React.Fragment>
  );
}
