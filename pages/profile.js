import React, { useContext, useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import View from '../components/View';
import AppHelper from '../app-helper';

export default function edit() {
  const { setUser } = useContext(UserContext);

  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');

  useEffect(() => {
    const options = {
      headers: { Authorization: `Bearer ${AppHelper.getAccessToken()}` },
    };
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details`,
      options
    )
      .then(AppHelper.toJSON)
      .then((userData) => {
        if (typeof userData._id !== 'undefined') {
          setUser({
            id: userData._id,
            isAdmin: userData.isAdmin,
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.email,
          });

          setfirstName(userData.firstName);
          setlastName(userData.lastName);
        }
      });
  }, []);

  function editProfile(e) {
    e.preventDefault();
    fetch(`https://agile-fortress-85999.herokuapp.com/users/edit-user`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: {
        firstName: firstName,
        lastName: lastName,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
  }

  console.log(firstName);
  return (
    <div>
      <View title={'Edit'}>
        <Row className="justify-content-center">
          <Col xs md="6">
            <h3>Edit User</h3>
          </Col>
        </Row>
      </View>

      <Card>
        <Card.Body>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group>
              <Form.Label>First Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder={firstName}
                onChange={(e) => setfirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Last Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder={lastName}
                onChange={(e) => setlastName(e.target.value)}
              />
            </Form.Group>

            <Button onClick={editProfile}> Submit </Button>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}
