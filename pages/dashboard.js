import React from 'react';
import Router from 'next/router';
import { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import UserContext from '../UserContext';

import { Jumbotron, Col, Row, Container, Button } from 'react-bootstrap';
import moment from 'moment';
import AppHelper from '../app-helper';
import Doughnut from '../components/DoughnutChart';
import LineChart from '../components/LineChart';
import Search from '../components/search';

export default function dashboard() {
  const { user, setUser } = useContext(UserContext);

  const [totalIncome, setTotalIncome] = useState(0);
  const [totalExpense, setTotalExpense] = useState(0);
  const [dateArray, setDateArray] = useState([]);
  const [incomeArray, setIncomeArray] = useState([]);
  const [expenseArray, setExpenseArray] = useState([]);
  const [total, setTotal] = useState(totalIncome, totalExpense);

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/records`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.length) {
          const dateArr = data.map((category) => {
            return new Date(category.inputOn).toLocaleString();
          });

          const incomeArr = data.map((category) => {
            if (category.categoryType === 'Income') {
              return category.amount;
            } else {
              return 0;
            }
          });

          const expenseArr = data.map((category) => {
            if (category.categoryType === 'Expense') {
              return category.amount;
            } else {
              return 0;
            }
          });

          setDateArray(dateArr);
          setIncomeArray(incomeArr);
          setExpenseArray(expenseArr);
        }
      });
  }, []);

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/records`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.length) {
          let totalin = 0;
          const incomeArray = data.filter((category) => {
            return category.categoryType === 'Income';
          });
          incomeArray.forEach((category) => {
            totalin += category.amount;
          });
          let totalex = 0;
          const expenseArr = data.filter((category) => {
            return category.categoryType === 'Expense';
          });
          expenseArr.forEach((category) => {
            totalex += category.amount;
          });

          setTotalIncome(totalin);
          setTotalExpense(totalex);
          setTotal(totalin - totalex);
        }
      });
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Dashboard</title>
      </Head>
      <Jumbotron className="lg-8 bg-info mt-4">
        <Container className="d-flex flex-column align-items-center">
          <div className="bg-dark mt-5 rounded">
            <h3 className="p-4 text-white text-center">
              Welcome back, {user.firstName} {user.lastName}!
            </h3>
          </div>
          <h4 className="text-center mt-2 bg-dark p-3 rounded text-white">
            You have {moment().daysInMonth() - moment().date()} days left for
            the month of {moment().format('MMMM YYYY')}.
          </h4>
          <h3 className="text-center mt-2 bg-dark p-3 rounded text-white">
            Budget Remaining: {AppHelper.formatAmount(total)}
          </h3>
        </Container>
        <Col auto>
          <Button
            className="rounded bg-dark float-right"
            onClick={() => Router.push('./addRecord')}
          >
            Add Transaction
          </Button>
        </Col>
        <Search />

        <Jumbotron>
          <Container>
            <Row>
              <Col>
                <h3>Total Income over Total Expense</h3>
                <Doughnut income={totalIncome} expense={totalExpense} />
              </Col>
              <Col>
                <h3>Income & Expense Over Time</h3>
                <LineChart
                  dateArray={dateArray}
                  incomeArray={incomeArray}
                  expenseArray={expenseArray}
                />
              </Col>
            </Row>
          </Container>
        </Jumbotron>
      </Jumbotron>
    </React.Fragment>
  );
}
