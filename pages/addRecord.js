import { Router } from 'next/router';
import { useState, useEffect, useContext } from 'react';
import {
  Form,
  Button,
  Container,
  Jumbotron,
  InputGroup,
} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import CategoryModal from '../components/addCategoryModal';

export default function addRecord() {
  const [categoryType, setCategoryType] = useState('Income');
  const [categoryName, setCategoryName] = useState('');
  const [amount, setAmount] = useState('');
  const [description, setDescription] = useState('');
  const [categoriesInRecord, setCategoriesInRecord] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const { token } = useContext(UserContext);

  function record(e) {
    e.preventDefault();

    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/new-record`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        categoryType: categoryType,
        categoryName: categoryName,
        amount: amount,
        description: description,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        if (data === true) {
          Swal.fire('Record added successfully!', '', 'success');
        } else {
          Swal.fire('Incomplete', '', 'warning');
        }
      });
  }

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/categories`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        const catNameArray = data.filter((category) => {
          console.log(data);
          if (data !== ['']) {
            if (category.categoryType === 'Income') {
              return category.categoryType === 'Income';
            } else {
              return category.categoryType === 'Expense';
            }
          } else {
            Router.push('/addCategory');
          }
        });

        const newArray = data.map((categoriesRecord) => {
          return (
            <option
              key={categoriesRecord._id}
              value={categoriesRecord.categoryName}
            >
              {categoriesRecord.categoryName}
            </option>
          );
        });

        setCategoriesInRecord(newArray);
      });
  }, [token, categoryType]);

  return (
    <Container className=" my-5">
      <Jumbotron className="lg-8 bg-info  mt-4 text-white">
        <h1 className="text-center">Add New Transaction</h1>
        <Form onSubmit={(e) => record(e)}>
          <Form.Group className="mt-2">
            <Form.Label>Record Description</Form.Label>
            <Form.Control
              type="description"
              placeholder="Description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mt-2">
            <Form.Label className="mt-2">Select Transaction Type</Form.Label>
            <Form.Control
              as="select"
              value={categoryType}
              onChange={(e) => setCategoryType(e.target.value)}
              required
            >
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="mt-2">Select Category </Form.Label>
            <InputGroup className="mt-2">
              <Form.Control
                as="select"
                value={categoryName}
                onChange={(e) => setCategoryName(e.target.value)}
                required
              >
                {categoriesInRecord}
              </Form.Control>

              <InputGroup.Append>
                <Button
                  className="bg-dark text-white"
                  variant="outline-secondary"
                  onClick={() => setModalShow(true)}
                >
                  Add Category
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Form.Group>
          <Form.Group className="mt-2">
            <Form.Label>Amount</Form.Label>
            <Form.Control
              type="number"
              placeholder="&#8369;"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              required
            />
          </Form.Group>
          <CategoryModal show={modalShow} onHide={() => setModalShow(false)} />
          <Button
            block
            className="bg-dark text-white"
            type="submit"
            id="submitBtn"
          >
            Submit
          </Button>
        </Form>
      </Jumbotron>
    </Container>
  );
}
