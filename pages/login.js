import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';

import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../app-helper';

export default function Login() {
  const { setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function redirect(e) {
    e.preventDefault();
    Router.push('/register');
  }

  function authenticate(e) {
    e.preventDefault();

    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          Router.push('/addRecord');
        }
      });
  }
  const authenticateGoogleToken = (response) => {
    const payload = {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({ tokenId: response.tokenId }),
    };
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/verify-google-id-token`,
      payload
    )
      .then(AppHelper.toJSON)
      .then((data) => {
        if (typeof data.accessToken !== 'undefined') {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
          return Router.push('/dashboard');
        } else {
          if (data.error === 'google-auth-error') {
            Swal.fire(
              'Google Auth Error',
              'Google authentication procedure failed',
              'error'
            );
          } else if (data.error === 'google-auth-error') {
            Swal.fire(
              'Login Type Error',
              'You may have registered through a different login procedure',
              'error'
            );
          }
        }
      });
  };

  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: { Authorization: `Bearer ${accessToken}` },
    };

    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details`,
      options
    )
      .then(AppHelper.toJSON)
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
        Router.push('/dashboard');
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Card className="mt-5">
      <Card.Header className="text-center text-white bg-dark">
        Login Details
      </Card.Header>
      <Card.Body>
        <Form onSubmit={(e) => authenticate(e)}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              className="rounded-pill"
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              className="rounded-pill"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          <div className="my-12 border-b text-center">
            <Button
              className="mt-3 mb-3 rounded-pill flex items-center justify-center"
              type="submit"
              block
            >
              Log In
            </Button>
          </div>
          <div className="my-12 border-b text-center">
            <GoogleLogin
              clientId="492852751994-5ovd6je0t63stcndl36hfj68317a3763.apps.googleusercontent.com"
              render={(renderProps) => (
                <Button
                  className="mt-3 bg-primary rounded-pill  flex items-center justify-center "
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                  block
                >
                  Login using Google Account
                </Button>
              )}
              buttonText="Google Login"
              onSuccess={authenticateGoogleToken}
              onFailure={authenticateGoogleToken}
              cookiePolicy={'single_host_origin'}
            />
          </div>
          <Form.Group>
            <p className="mt-3 mb-1">No account yet?</p>

            <Button
              variant="outline-primary"
              className="mt-3 mb-3 bg-indigo-500 rounded-pill hover:bg-indigo-700  flex items-center justify-center "
              block
              onClick={(e) => redirect(e)}
            >
              <a>Sign up here</a>
            </Button>
          </Form.Group>
        </Form>
      </Card.Body>
    </Card>
  );
}
