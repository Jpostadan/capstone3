import React from 'react';
import Head from 'next/head';
import { Container, Jumbotron } from 'react-bootstrap';

import Login from './login';

export default function Home() {
  return (
    <React.Fragment>
      <Head>
        <title>Expense Tracker</title>
      </Head>

      <Container>{/* <Component {...pageProps} /> */}</Container>
      <Jumbotron className="lg-8 bg-info mt-4">
        <h1 className="text-center">Track your Income and Expenses</h1>

        <Login />
      </Jumbotron>
    </React.Fragment>
  );
}
