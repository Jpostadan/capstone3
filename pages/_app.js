import React, { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';
import Navbar from '../components/NavBar';

const initialUser = {
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  categories: [],
  records: [],
};

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState(initialUser);
  useEffect(() => {
    fetch(`https://agile-fortress-85999.herokuapp.com/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          categories: data.categories,
          records: data.records,
        });
      });
  }, []);
  const unsetUser = () => {
    localStorage.clear();
    setUser(initialUser);
  };
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Navbar />
      <Container>
        <Component {...pageProps} />
      </Container>
    </UserProvider>
  );
}
export default MyApp;
