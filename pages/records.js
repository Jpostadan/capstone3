import { useState, useEffect } from 'react';
import { Form, Container, Jumbotron, Table, Button } from 'react-bootstrap';
import Router from 'next/router';
import moment from 'moment';

export default function getRecords() {
  const [records, setRecords] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/records`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        let number = 0;
        const newArray = data.map((newRecord) => {
          number++;
          return (
            <tr key={newRecord._id}>
              <td>{number}</td>
              <td>{newRecord.description}</td>
              <td>{newRecord.categoryName}</td>
              <td>{newRecord.categoryType}</td>
              <td>{newRecord.amount}</td>
              <td>{moment(newRecord.inputOn).format('MMMM DD, YYYY')}</td>
            </tr>
          );
        });
        setRecords(newArray);
      });
  }, []);
  const addTransaction = () => {
    Router.push('/addRecord');
  };

  return (
    <Jumbotron className="lg-8 bg-info mt-4 text-white">
      <>
        <h1 className="my-3 text-center">Records List</h1>
      </>

      <Container>
        <Form.Group>
          <Button className="mt-3" onClick={addTransaction}>
            Add Record
          </Button>
          <Table className="fontsize" striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Record Description</th>
                <th>Category Name</th>
                <th>Category Type</th>
                <th>Amount</th>
                <th>Date Added</th>
              </tr>
            </thead>
            {records !== null ? (
              <tbody>{records}</tbody>
            ) : (
              <Alert variant="info">No Records Found</Alert>
            )}
          </Table>
        </Form.Group>
      </Container>
    </Jumbotron>
  );
}
