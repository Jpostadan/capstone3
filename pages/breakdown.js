import React from 'react';
import colorRandomizer from '../helpers/ColorRandomizer';
import LineChart from '../components/LineChart';
import PieChart from '../components/PieChart';

import { useState, useEffect } from 'react';
import { Container, Jumbotron, Card } from 'react-bootstrap';
// import { Pie } from 'react-chartjs-2';

export default function breakdown() {
  const [dateArray, setDateArray] = useState([]);
  const [incomeArray, setIncomeArray] = useState([]);
  const [expenseArray, setExpenseArray] = useState([]);
  const [label, setLabel] = useState([]);
  const [inLabel, setInLabel] = useState([]);
  const [bgColors, setBgColors] = useState([]);

  useEffect(() => {
    fetch(
      `https://agile-fortress-85999.herokuapp.com/api/users/details/records`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.length) {
          const label = data.map((category) => {
            return category.categoryName;
          });

          const dateArr = data.map((category) => {
            console.log(category.categoryType);
            return new Date(category.inputOn).toLocaleString();
          });

          const incomeArr = data.map((category) => {
            console.log(category.amount);
            if (category.categoryType === 'Income') {
              return category.amount;
            } else {
              return 0;
            }
          });
          const inLabel = data.map((category) => {
            if (category.categoryType === 'Income') {
              return category.categoryName;
            } else {
              return 0;
            }
          });

          const expenseArr = data.map((category) => {
            if (category.categoryType === 'Expense') {
              return category.amount;
            } else {
              return 0;
            }
          });

          setDateArray(dateArr);
          setIncomeArray(incomeArr);
          setExpenseArray(expenseArr);
          setLabel(label);
          setInLabel(inLabel);
        }
      });
  }, []);
  useEffect(() => {
    setBgColors(inLabel.map(() => `#${colorRandomizer()}`));
  }, [inLabel]);

  const dataPie = {
    labels: inLabel,
    datasets: [
      {
        data: incomeArray,
        expenseArray,
        backgroundColor: bgColors,
        hoverBackgroundColor: bgColors,
      },
    ],
  };

  return (
    <React.Fragment>
      <Jumbotron className="lg-8 bg-info  mt-4 text-white">
        <Container>
          <h1 className="text-center my-3">Income vs. Expense</h1>
          <Jumbotron>
            <PieChart
              label={label}
              incomeArray={incomeArray}
              expenseArray={expenseArray}
            />
          </Jumbotron>
          <Jumbotron>
            <LineChart
              dateArray={dateArray}
              incomeArray={incomeArray}
              expenseArray={expenseArray}
            />
          </Jumbotron>

          {/* <h1 className="text-center">Income</h1>
        <Pie data={dataPie} /> */}
        </Container>
      </Jumbotron>
    </React.Fragment>
  );
}
