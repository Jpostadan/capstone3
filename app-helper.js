module.exports = {
  API_URL: process.env.NEXT_PUBLIC_API_URL,
  getAccessToken: () => localStorage.getItem('token'),
  toJSON: (response) => response.json(),
  formatAmount: (num) =>
    new Intl.NumberFormat('en-PH', {
      style: 'currency',
      currency: 'PHP',
    }).format(num),
};
